CREATE TABLE IF NOT EXISTS Guilds (
    ID          BIGINT UNSIGNED PRIMARY KEY NOT NULL
);

CREATE TABLE IF NOT EXISTS Users (
    ID          BIGINT UNSIGNED PRIMARY KEY NOT NULL
);

CREATE TABLE IF NOT EXISTS Quotes (
    Name        CHAR(255) PRIMARY KEY NOT NULL,
    Content     VARCHAR(2000) NOT NULL
);

CREATE TABLE IF NOT EXISTS Roles (
    GuildID     BIGINT UNSIGNED NOT NULL,
	Vetted      BIGINT UNSIGNED,
	FOREIGN KEY(GuildID) REFERENCES Guilds(ID)
);