// A Discord bot focused on addressing the inherent problems with Discord, to
// allow a more socialist/anarchist organization of servers (or "guilds").
// Copyright (C) 2021 Logan Wemyss

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use std::env;
use std::time::Duration;

use anyhow::{Context, Result};
use poise::serenity_prelude::GatewayIntents;
use tokio::signal;

use aca_bot::{commands, db};

#[tokio::main]
async fn main() -> Result<()> {
    dotenv::dotenv()?;

    let mut options = poise::FrameworkOptions {
        commands: vec![
            commands::general::echo(),
            commands::general::chad(),
            commands::general::source(),
            commands::quotes::quote(),
            commands::general::age(),
            commands::init::init(),
            commands::vet::vet(),
        ],
        prefix_options: poise::PrefixFrameworkOptions {
            prefix: Some("!".into()),
            edit_tracker: Some(poise::EditTracker::for_timespan(Duration::from_secs(3600))),
            additional_prefixes: vec![],
            ..Default::default()
        },
        on_error: |error| Box::pin(aca_bot::error::on_error(error)),
        pre_command: |ctx| {
            Box::pin(async move {
                println!("Executing command {}...", ctx.command().qualified_name);
            })
        },
        post_command: |ctx| {
            Box::pin(async move {
                println!("Executed command {}!", ctx.command().qualified_name);
            })
        },
        command_check: Some(|_| Box::pin(async move { Ok(true) })),
        listener: |ctx, event, framework, data| {
            Box::pin(async move {
                println!("Got an event in listener: {:?}", event.name());
                aca_bot::events::handle_event(ctx, event, &framework, data).await
            })
        },
        ..Default::default()
    };

    for command in &mut options.commands {
        command.required_bot_permissions |= command.required_permissions;
    }

    let database = db::new(
        env::var("DATABASE_URL").context("Fill out .env")?,
        env::var("MAX_CONNECTIONS")
            .context("Fill out .env")?
            .parse()?,
    )
    .await?;

    let client = poise::Framework::builder()
        .token(env::var("DISCORD_TOKEN").context("Fill out .env")?)
        .user_data_setup(move |_, _, _| Box::pin(async move { Ok(database.clone()) }))
        .options(options)
        .intents(
            GatewayIntents::GUILD_MEMBERS
                | GatewayIntents::GUILD_BANS
                | GatewayIntents::GUILD_MESSAGES
                | GatewayIntents::GUILD_MESSAGE_TYPING
                | GatewayIntents::GUILD_MESSAGE_REACTIONS
                | GatewayIntents::DIRECT_MESSAGES
                | GatewayIntents::DIRECT_MESSAGE_REACTIONS
                | GatewayIntents::GUILDS
                | GatewayIntents::GUILD_PRESENCES
                | GatewayIntents::GUILD_INTEGRATIONS
                | GatewayIntents::GUILD_INVITES
                | GatewayIntents::MESSAGE_CONTENT
                | GatewayIntents::GUILD_PRESENCES
                | GatewayIntents::GUILD_SCHEDULED_EVENTS,
        )
        .build()
        .await?;

    loop {
        tokio::select! {
            _ = signal::ctrl_c() => {return Err(anyhow::anyhow!("\nAborting program"));}
            result = client.clone().start_autosharded() => {result?;}
        };
    }
}
