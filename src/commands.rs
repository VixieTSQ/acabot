// A Discord bot focused on addressing the inherent problems with Discord, to
// allow a more socialist/anarchist organization of servers (or "guilds").
// Copyright (C) 2021 Logan Wemyss

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

pub mod general;
pub mod init;
pub mod quotes;
pub mod vet;

use std::time::Duration;

/// Evaluate mutally exclusive buttons to one button interaction.
#[macro_export]
macro_rules! eval_buttons {
    ($msg:expr, $ctx:expr, $( $id:expr ),+ ) => ({
        loop {
            if let Some(interaction) =
            $msg.await_component_interaction($ctx.discord())
                .author_id($ctx.author().id)
                .channel_id($ctx.channel_id())
                .collect_limit(1)
                .timeout(Duration::from_secs(3600))
                .await
            {
                if $( interaction.data.custom_id == $id )||+ {
                    break interaction
                }
            }
        }
    });
}
