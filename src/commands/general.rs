// A Discord bot focused on addressing the inherent problems with Discord, to
// allow a more socialist/anarchist organization of servers (or "guilds").
// Copyright (C) 2021 Logan Wemyss

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! General purpose isolated commands that do not affect state or the such

use std::env;

use anyhow::Result;

use crate::*;

/// Use the bot to repeat a message
#[poise::command(prefix_command, slash_command)]
pub async fn echo(
    ctx: Context<'_>,
    #[rest]
    #[description = "Message"]
    msg: Option<String>,
) -> Result<()> {
    if msg.is_none() {
        ctx.say("echo . . . echo . . . . . echo . . . . . . .")
            .await?;
    } else {
        ctx.say(msg.unwrap()).await?;
    }

    Ok(())
}

/// Retrieve the bot's source code
#[poise::command(prefix_command, slash_command)]
pub async fn source(ctx: Context<'_>) -> Result<()> {
    ctx.say(
        &env::var("REPOSITORY_LINK")
            .unwrap_or_else(|_| "No link! Your rights are being infringed on!".to_owned()),
    )
    .await?;

    Ok(())
}

/// Are you a chad?
#[poise::command(prefix_command, slash_command)]
pub async fn chad(ctx: Context<'_>) -> Result<()> {
    ctx.say(if ctx.author().name.to_lowercase().contains('e') {
        "You are a chad."
    } else {
        "You are not a chad."
    })
    .await?;

    Ok(())
}

/// Show the age of your discord account
#[poise::command(slash_command, prefix_command)]
pub async fn age(
    ctx: Context<'_>,
    #[description = "Selected user"] user: Option<serenity::User>,
) -> Result<()> {
    let u = user.as_ref().unwrap_or_else(|| ctx.author());
    let response = format!("{}'s account was created at {}", u.name, u.created_at());
    ctx.say(response).await?;
    Ok(())
}
