// A Discord bot focused on addressing the inherent problems with Discord, to
// allow a more socialist/anarchist organization of servers (or "guilds").
// Copyright (C) 2021 Logan Wemyss

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use anyhow::Result;
use poise::CreateReply;
use poise::serenity_prelude::{ButtonStyle, InteractionResponseType};

use crate::*;
use crate::models::DBGuild;
use super::*;

/// Interactively initialize a guild
#[poise::command(
    prefix_command, 
    slash_command,
    guild_only,
    ephemeral,
    required_permissions = "MANAGE_CHANNELS | MANAGE_ROLES",
    required_bot_permissions = "MANAGE_MESSAGES"
)]
pub async fn init(ctx: Context<'_>) -> Result<()> {
    todo!();
    let conn = ctx.data();

    // check if the guild is already initialized, ask the user if they want to reinitialize
    let guild = DBGuild::get(&conn, ctx.guild().unwrap().id.0).await?.unwrap();

    let mut message = CreateReply {
        ephemeral: ctx.command().ephemeral,
        allowed_mentions: ctx.framework().options().allowed_mentions.clone(),

        ..CreateReply::default()
    };
    
    let message = message.embed(|embed| 
        embed.title("Initialize guild")
    );

    let invoc_id = ctx.id().to_string();

    // TODO: Change back to Initialized
    if let DBGuild::Uninitialized { .. } = guild {
        let reinit_id = invoc_id.clone() + "reinit";
        let cancel_id = invoc_id.clone() + "cancel";
        
        message.embeds[0]
            .description("This guild is already initialized. Would you like to reinitialize it?");
        message.components(|comp| comp.create_action_row(|action| 
                action.create_button(|button| 
                    button.label("Reinitialize")
                        .style(ButtonStyle::Success)
                        .custom_id(reinit_id.clone())
                )
                    .create_button(|button| 
                        button.label("Cancel")
                            .style(ButtonStyle::Danger)
                            .custom_id(cancel_id.clone())
                    )
            )
        );
        let reply_handle = poise::send_reply(ctx, |m| { *m = message.clone(); m }).await?; 
        let interaction = eval_buttons!(reply_handle.message().await?, ctx, reinit_id, cancel_id);

        reply_handle.into_message().await?.delete(ctx.discord()).await?;
        match &interaction.data.custom_id {
            id if id == &reinit_id => {

                message.components(|comp| comp);
                message.embeds[0].description("");

                // TODO: uncomment these
                // let conn = conn.acquire();
                // guild.remove(&mut conn.await?).await?;
            },
            id if id == &cancel_id => {
                return Ok(());
            },
            _ => unreachable!()
        };
    };

    {
        let auto_id = invoc_id.clone() + "auto";
        let existing_id = invoc_id.clone() + "existing";

        message.components(|comp| comp.create_action_row(|action| 
            action.create_button(|button| 
                button.label("Auto")
                    .custom_id(auto_id.clone())
            )
                .create_button(|button| 
                    button.label("Existing")
                        .custom_id(existing_id.clone())
                )
            )
        );

        let reply_handle = ctx.send(|m| { *m = message.clone(); m }).await?;
        let existing_role_interaction = eval_buttons!(reply_handle.message().await?, ctx, auto_id, existing_id);

        match &existing_role_interaction.data.custom_id {
            id if id == &existing_id => {
                let input_id = ctx.id().to_string() + "input";

                message.components(|comp| comp.create_action_row(|action|
                        action.create_input_text(|input|
                            input.label("Role")
                                .required(true)
                                .custom_id(input_id.clone())

                        )
                    )
                );
                message.embeds[0].description("Input the role name or ID for vetted users.");

                reply_handle.edit(ctx, |msg|
                    msg.embed(|embed| {
                        *embed = message.embeds[0].clone();
                        embed
                    })
                ).await?;

                existing_role_interaction.create_interaction_response(ctx.discord(), |res|
                    res.kind(InteractionResponseType::DeferredUpdateMessage)
                ).await?;
            },
            id if id == &auto_id => {
                message.embeds[0].description("");
            },
            _ => unreachable!()
        }
    };

    /*
    let vet_role = todo!(); // ask for vetted role, or create one
    let vet_role = match vet_role {
        Some(role) => role, // do nothing
        None => todo!(), // create a role
    };

    // create vetting category
    todo!();
    // put into new dbguild struct

    // get or create vetted role
    let vet_role = todo!(); // ask for vetted role, or create one
    let vet_role = match vet_role {
        Some(role) => role, // do nothing
        None => todo!(), // create a role
    };
    // put role into new dbguild struct

    // get vetting text 
    let vetting_text = todo!(); // ask for vetting text
    // put vetting text into new dbguild struct

    // add new dbguild struct to db

    // Done!
    */

    Ok(())
}