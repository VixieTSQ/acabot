// A Discord bot focused on addressing the inherent problems with Discord, to
// allow a more socialist/anarchist organization of servers (or "guilds").
// Copyright (C) 2021 Logan Wemyss

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use anyhow::Result;
use poise::serenity_prelude::{ArgumentConvert, ChannelId, Member};

use crate::{models::*, *};

/// Show the age of your discord account
#[poise::command(
    slash_command,
    prefix_command,
    ephemeral,
    subcommands("accept", "kick", "ban")
)]
pub async fn vet(ctx: Context<'_>) -> Result<()> {
    ctx.say("**Usage:**\n`!vet accept`\n`!vet kick`\n`!vet ban`")
        .await?;
    Ok(())
}

/// Accept this vetting ticket and vet the user
#[poise::command(
    slash_command,
    prefix_command,
    guild_only,
    ephemeral,
    required_permissions = "MANAGE_CHANNELS | MANAGE_ROLES"
)]
pub async fn accept(ctx: Context<'_>) -> Result<()> {
    let conn = ctx.data();
    let ticket =
        match Ticket::get_by_channel(conn, ctx.guild_id().unwrap().0, ctx.channel_id().0).await? {
            Some(ticket) => ticket,
            None => {
                ctx.say("This is not a ticket channel").await?;
                return Ok(());
            }
        };
    let channel = ChannelId(ticket.channel);
    let ticket_user_id = ticket.user_id.to_string();
    let mut ticket_user =
        Member::convert(ctx.discord(), ctx.guild_id(), None, &ticket_user_id).await?;
    let vetted_role = Roles::get(conn, ctx.guild_id().unwrap().0)
        .await?
        .unwrap()
        .vetted;
    ticket_user.add_role(ctx.discord(), vetted_role).await?;
    ticket.remove(conn).await?;
    channel.delete(ctx.discord()).await?;
    Ok(())
}

/// Reject this vetting ticket and kick the user
#[poise::command(
    slash_command,
    prefix_command,
    guild_only,
    ephemeral,
    required_permissions = "KICK_MEMBERS",
    required_bot_permissions = "MANAGE_CHANNELS"
)]
pub async fn kick(ctx: Context<'_>) -> Result<()> {
    let conn = ctx.data();
    let ticket =
        match Ticket::get_by_channel(conn, ctx.guild_id().unwrap().0, ctx.channel_id().0).await? {
            Some(ticket) => ticket,
            None => {
                ctx.say("This is not a ticket channel").await?;
                return Ok(());
            }
        };
    let ticket_user_id = ticket.user_id.to_string();
    let ticket_user = Member::convert(ctx.discord(), ctx.guild_id(), None, &ticket_user_id).await?;
    ticket_user
        .kick_with_reason(ctx.discord(), "Did not pass vetting")
        .await?;
    Ok(())
}

/// Reject this vetting ticket and ban the user
#[poise::command(
    slash_command,
    prefix_command,
    guild_only,
    ephemeral,
    required_permissions = "BAN_MEMBERS",
    required_bot_permissions = "MANAGE_CHANNELS"
)]
pub async fn ban(ctx: Context<'_>) -> Result<()> {
    let conn = ctx.data();
    let ticket =
        match Ticket::get_by_channel(conn, ctx.guild_id().unwrap().0, ctx.channel_id().0).await? {
            Some(ticket) => ticket,
            None => {
                ctx.say("This is not a ticket channel").await?;
                return Ok(());
            }
        };
    let ticket_user_id = ticket.user_id.to_string();
    let ticket_user = Member::convert(ctx.discord(), ctx.guild_id(), None, &ticket_user_id).await?;
    ticket_user
        .ban_with_reason(ctx.discord(), 0, "Did not pass vetting")
        .await?;
    Ok(())
}
