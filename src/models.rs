// A Discord bot focused on addressing the inherent problems with Discord, to
// allow a more socialist/anarchist organization of servers (or "guilds").
// Copyright (C) 2021 Logan Wemyss

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! Modeling data such as users or guilds.
//! Separate from serenity's models as these are supposed to be derived from the
//! database.

mod ticket;
mod user;
mod guild;
mod roles;
mod channel;

pub use ticket::*;
pub use user::*;
pub use guild::*;
pub use roles::*;
pub use channel::*;
