// A Discord bot focused on addressing the inherent problems with Discord, to
// allow a more socialist/anarchist organization of servers (or "guilds").
// Copyright (C) 2021 Logan Wemyss

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! A Discord bot focused on addressing the inherent problems with Discord, to
//! allow a more socialist/anarchist organization of servers (or "guilds").

use std::borrow::Borrow;

use poise::serenity_prelude as serenity;
use serenity::model::id;
use sqlx::MySqlPool;

pub mod commands;
pub mod db;
pub mod error;
pub mod events;
pub mod models;

type Context<'a> = poise::Context<'a, MySqlPool, anyhow::Error>;

/// Gives you all the guilds that the bot is in inside of for a given user.
/// For example, if you provide a user foobar#4242 then it will give back a vec
/// of all the guilds that user is in (that the bot has access to).
pub async fn guilds_user_in(ctx: &serenity::Context, user: impl Borrow<id::UserId>) -> Vec<id::GuildId>
{
    let user = user.borrow();
    let mut guilds = Vec::new();
    for guild in ctx.cache.guilds() {
        let contains_user = ctx
            .cache
            .guild_field(guild, |g| g.members.contains_key(&user));
        match contains_user {
            Some(contains_user) => {
                if contains_user {
                    guilds.push(guild);
                }
            }
            None => (),
        }
    }

    guilds
}
