// A Discord bot focused on addressing the inherent problems with Discord, to
// allow a more socialist/anarchist organization of servers (or "guilds").
// Copyright (C) 2021 Logan Wemyss

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use poise::serenity_prelude::GuildId;
use sqlx::{query, MySqlExecutor, MySqlPool};
use anyhow::Result;

pub enum DBGuild {
    Uninitialized (GuildId),
    Initialized {
        id: GuildId,
        roles: super::Roles,
        vetting_text: String
    }
}

impl DBGuild {
    pub fn new(id: u64) -> Self {
        Self::Uninitialized(GuildId(id))
    }

    /// Get a guild by a guild ID.
    pub async fn get(conn: &MySqlPool, id: u64) -> Result<Option<Self>>
    {
        let raw = query!("SELECT ID, VettingText FROM Guilds WHERE ID = ?", id)
            .fetch_optional(conn)
            .await?;

        let roles = { super::Roles::get(conn, id).await? };

        Ok(
            match raw {
                Some(f) => Some( 
                    match (roles, f.VettingText) {
                        (Some(r), Some(s)) => Self::Initialized {
                            id: GuildId(f.ID),
                            roles: r,
                            vetting_text: s,
                        },
                        _ => Self::Uninitialized(GuildId(f.ID)),
                    }
                ),
                None => None,
            }
        )
    }

    /// Update the Guilds table with a GuildID.
    /// Does not overwrite existing entries.
    pub async fn add(self, conn: impl MySqlExecutor<'_>) -> Result<()> {
        query!("INSERT IGNORE INTO Guilds(id) VALUES (?)", self.get_id().0)
            .execute(conn)
            .await?;

        Ok(())
    }

    /// Update the Guilds table with a guild id.
    /// Removes corresponding entries.
    pub async fn remove(self, conn: impl MySqlExecutor<'_>) -> Result<()> {
        query!("DELETE FROM Guilds WHERE ID = (?)", self.get_id().0)
            .execute(conn)
            .await?;

        Ok(())
    }

    /// Get the guild ID.
    pub fn get_id(&self) -> GuildId {
        match self {
            DBGuild::Uninitialized(id) => *id,
            DBGuild::Initialized { id, .. } => *id,
        }
    }
}