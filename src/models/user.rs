// A Discord bot focused on addressing the inherent problems with Discord, to
// allow a more socialist/anarchist organization of servers (or "guilds").
// Copyright (C) 2021 Logan Wemyss

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use anyhow::Result;
use poise::serenity_prelude as serenity;
use poise::serenity_prelude::{id, UserId};
use sqlx::{query, MySqlExecutor, MySqlPool};

use super::DBGuild::*;

pub struct DBUser {
    pub id: UserId,
}

impl DBUser {
    pub fn new(id: u64) -> Self {
        Self { id: UserId(id) }
    }

    /// Get a user from a user ID and serenity context.
    pub async fn get(conn: impl MySqlExecutor<'_>, id: u64) -> Result<Option<Self>> {
        let raw = query!("SELECT ID FROM Users WHERE ID = ?", id)
            .fetch_optional(conn)
            .await?;

        Ok(raw.map(|f| Self { id: UserId(f.ID) }))
    }

    /// Update the Users table with a UserId.
    /// Does not overwrite existing entries.
    pub async fn add(self, conn: impl MySqlExecutor<'_>) -> Result<()> {
        query!("INSERT IGNORE INTO Users(id) VALUES (?)", self.id.0)
            .execute(conn)
            .await?;

        Ok(())
    }

    /// Update the Users table with a user id.
    /// Removes corresponding entries.
    pub async fn remove(self, conn: impl MySqlExecutor<'_>) -> Result<()> {
        query!("DELETE FROM Users WHERE ID = (?)", self.id.0)
            .execute(conn)
            .await?;

        Ok(())
    }

    /// Gives you all the guilds that the bot is in inside of for a given user.
    /// For example, if you provide a user foobar#4242 then it will give back a vec
    /// of all the guilds that user is in (that the bot has access to).
    pub async fn guilds_in(&self, ctx: &serenity::Context) -> Vec<id::GuildId> {
        let mut guilds = Vec::new();
        for guild in ctx.cache.guilds() {
            let contains_user = ctx
                .cache
                .guild_field(guild, |g| g.members.contains_key(&self.id));
            match contains_user {
                Some(contains_user) => {
                    if contains_user {
                        guilds.push(guild);
                    }
                }
                None => (),
            }
        }

        guilds
    }

    pub async fn vetted_in(
        &self,
        conn: &MySqlPool,
        ctx: &serenity::Context,
    ) -> Result<Vec<super::DBGuild>> {
        let mut vetted_in = vec![];

        for guild in self.guilds_in(&ctx).await {
            match super::DBGuild::get(conn, guild.0).await? {
                Some(guild) => {
                    let vet_id = match &guild {
                        Initialized { id: _, roles, .. } => roles.vetted,
                        Uninitialized(..) => continue,
                    };
                    let res = self
                        .id
                        .to_user(&ctx)
                        .await?
                        .has_role(&ctx, guild.get_id(), vet_id)
                        .await?;

                    if res {
                        vetted_in.push(guild);
                    };
                }
                None => continue,
            };
        }

        Ok(vetted_in)
    }
}
