// A Discord bot focused on addressing the inherent problems with Discord, to
// allow a more socialist/anarchist organization of servers (or "guilds").
// Copyright (C) 2021 Logan Wemyss

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use anyhow::Result;
use sqlx::{query, MySqlExecutor};

#[derive(Default)]
pub struct Roles {
    pub vetted: u64,
}

impl Roles {
    pub fn new() -> Self {
        Self::default()
    }

    /// Get a roles object by a guild ID.
    pub async fn get(conn: impl MySqlExecutor<'_>, id: u64) -> Result<Option<Self>> {
        let raw = query!("SELECT GuildID, Vetted FROM Roles WHERE GuildID = ?", id)
            .fetch_optional(conn)
            .await?;

        Ok(raw.map(|f| Self { vetted: f.Vetted }))
    }
}
