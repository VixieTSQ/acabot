// A Discord bot focused on addressing the inherent problems with Discord, to
// allow a more socialist/anarchist organization of servers (or "guilds").
// Copyright (C) 2021 Logan Wemyss

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

use anyhow::Result;
use poise::serenity_prelude::id;
use sqlx::{query, MySqlExecutor};

#[derive(Default)]
pub struct Ticket {
    pub channel: u64,
    pub guild_id: u64,
    pub user_id: u64,
}

impl Ticket {
    /// Gets a ticket by a guild ID and user ID.
    pub async fn get(
        conn: impl MySqlExecutor<'_>,
        guild_id: u64,
        user_id: u64,
    ) -> Result<Option<Self>> {
        let raw = query!(
            "SELECT GuildID, ChannelID FROM Tickets WHERE GuildID = ? AND UserID = ?",
            guild_id,
            user_id
        )
        .fetch_optional(conn)
        .await?;

        Ok(raw.map(|raw| Ticket {
            channel: raw.ChannelID,
            guild_id,
            user_id,
        }))
    }
    /// Gets a ticket by a guild ID and channel ID.
    pub async fn get_by_channel(
        conn: impl MySqlExecutor<'_>,
        guild_id: u64,
        channel_id: u64,
    ) -> Result<Option<Self>> {
        let raw = query!(
            "SELECT UserID FROM Tickets WHERE GuildID = ? AND ChannelID = ?",
            guild_id,
            channel_id
        )
        .fetch_optional(conn)
        .await?;

        Ok(raw.map(|raw| Ticket {
            channel: channel_id,
            guild_id,
            user_id: raw.UserID,
        }))
    }

    /// Update the Tickets table with a channel and guild id.
    /// Does not overwrite existing entries.
    pub async fn add(
        self,
        conn: impl MySqlExecutor<'_>,
        guild: id::GuildId,
        user_id: u64,
    ) -> Result<()> {
        query!(
            "INSERT IGNORE INTO Tickets(GuildID, ChannelID, UserID) VALUES (?,?,?)",
            guild.0,
            self.channel,
            user_id
        )
        .execute(conn)
        .await?;

        Ok(())
    }

    /// Update the Tickets table with a channel id.
    /// Removes corresponding entries.
    pub async fn remove(self, conn: impl MySqlExecutor<'_>) -> Result<()> {
        query!("DELETE FROM Tickets WHERE ChannelID = (?)", self.channel)
            .execute(conn)
            .await?;

        Ok(())
    }
}
