// A Discord bot focused on addressing the inherent problems with Discord, to
// allow a more socialist/anarchist organization of servers (or "guilds").
// Copyright (C) 2021 Logan Wemyss

// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as
// published by the Free Software Foundation, either version 3 of the
// License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.

// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

//! Handle various events called by serenity.

use std::borrow::Borrow;
use std::env;

use anyhow::{Context as _, Result};
use poise::serenity_prelude::{
    ChannelId, ChannelType, Mentionable, PermissionOverwrite, PermissionOverwriteType, Permissions,
};
use poise::{Event, FrameworkContext};
use serenity::model::guild::Member;
use serenity::model::id::GuildId;
use serenity::Context;
use sqlx::query;

use crate::models::*;
use crate::*;

pub async fn handle_event(
    ctx: &serenity::Context,
    event: &poise::Event<'_>,
    framework: &FrameworkContext<'_, MySqlPool, anyhow::Error>,
    database: &MySqlPool,
) -> Result<()> {
    match event {
        Event::Ready { .. } => {
            println!("Connected to Discord.");

            let commands = &framework.options().commands;

            let create_commands = poise::builtins::create_application_commands(commands);
            match env::var("DEVELOPMENT_SERVER_ID") {
                Ok(id) => {
                    let id = id.parse().context("Invalid development server ID")?;
                    let guild = GuildId(id);
                    println!(
                        "Registering slash commands locally in {}",
                        guild
                            .name(&ctx)
                            .ok_or(anyhow::anyhow!("Invalid development server ID"))?
                    );
                    guild
                        .set_application_commands(ctx, |b| {
                            *b = create_commands;
                            b
                        })
                        .await?;
                }
                Err(_) => {
                    println!("Registering slash commands globally");
                    serenity::Command::set_global_application_commands(ctx, |b| {
                        *b = create_commands;
                        b
                    })
                    .await?;
                }
            }
            Ok(())
        }
        Event::CacheReady { .. } => {
            println!("Updating DB.");
            update_users(ctx, database).await?;
            update_guilds(ctx, database).await?;
            println!("Done! Ready to go.");
            Ok(())
        }
        Event::GuildCreate { guild, .. } => DBGuild::new(guild.id.0).add(database).await,
        Event::GuildMemberAddition { new_member, .. } => {
            DBUser::new(new_member.user.id.0).add(database).await?;
            let guild = DBGuild::get(database, new_member.guild_id.0).await?;
            // TODO: DBGuild should contain a DBChannels
            if let Some(DBGuild::Initialized {
                id,
                roles: _,
                vetting_text,
            }) = guild
            {
                let channels = Channels::get(database, id.0).await?.unwrap();
                let permissions = vec![PermissionOverwrite {
                    allow: Permissions::VIEW_CHANNEL | Permissions::SEND_MESSAGES,
                    deny: Permissions::empty(),
                    kind: PermissionOverwriteType::Member(new_member.user.id),
                }];
                let new_channel = new_member
                    .guild_id
                    .create_channel(ctx, |cc| {
                        cc.category(channels.vetting)
                            .kind(ChannelType::Text)
                            .name(new_member.user.name.clone())
                            .permissions(permissions)
                    })
                    .await?;
                let ticket = Ticket {
                    channel: new_channel.id.0,
                    guild_id: new_member.guild_id.0,
                    user_id: new_member.user.id.0,
                };
                ticket
                    .add(database, new_member.guild_id, new_member.user.id.0)
                    .await?;
                new_channel
                    .say(
                        ctx,
                        format!("{}\n{}", vetting_text, new_member.user.mention()),
                    )
                    .await?;
            };
            Ok(())
        }
        Event::GuildMemberRemoval { user, guild_id, .. } => {
            let res = DBUser::get(database, user.id.0).await?;
            match res {
                Some(user) => {
                    if let Some(ticket) = Ticket::get(database, guild_id.0, user.id.0).await? {
                        let channel = ChannelId(ticket.channel);
                        ticket.remove(database).await?;
                        channel.delete(ctx).await?;
                    };
                    user.remove(database).await
                }
                None => Ok(()),
            }
        }
        Event::GuildUnavailable { guild_id } => {
            let res = DBGuild::get(database, guild_id.0).await?;
            match res {
                Some(guild) => guild.remove(database).await,
                None => Ok(()),
            }
        }
        _ => Ok(()),
    }
}

async fn update_users(ctx: impl Borrow<Context>, database: impl Borrow<MySqlPool>) -> Result<()> {
    let ctx = ctx.borrow();
    let mut members: Vec<Member> = vec![];
    for guild in ctx.cache.guilds() {
        guild
            .members(&ctx, None, None)
            .await?
            .into_iter()
            .map(|member| members.push(member))
            .len();
    }
    let ids = members.into_iter().map(|member| member.user.id);

    let mut conn = database.borrow().acquire().await?;
    let ids: Vec<u64> = ids.map(|id| id.0).collect();
    for id in &ids {
        query!("INSERT IGNORE INTO Users (ID) VALUES (?)", id)
            .execute(&mut conn)
            .await?;
    }
    if ids.len() == 0 {
        return Ok(());
    }
    let params = "ID=? AND ".to_string().repeat(ids.len() - 1);
    let query = &format!("DELETE FROM Users WHERE NOT {}{}", &params[..], "id=?")[..];

    let mut query = sqlx::query(query);
    for id in ids {
        query = query.bind(id);
    }
    query.execute(&mut conn).await?;

    Ok(())
}

async fn update_guilds(ctx: impl Borrow<Context>, database: impl Borrow<MySqlPool>) -> Result<()> {
    let ctx = ctx.borrow();
    let ids = ctx.cache.guilds();
    let mut conn = database.borrow().acquire().await?;
    let ids: Vec<u64> = ids.into_iter().map(|id| id.0).collect();
    for id in &ids {
        query!("INSERT IGNORE INTO Guilds (ID) VALUES (?)", id)
            .execute(&mut conn)
            .await?;
    }

    let params = "id=? AND ".to_string().repeat(ids.len() - 1);
    let query = &format!("DELETE FROM Guilds WHERE NOT {}{}", &params[..], "ID=?")[..];

    let mut query = sqlx::query(query);
    for id in ids {
        query = query.bind(id);
    }
    query.execute(&mut conn).await?;

    Ok(())
}
